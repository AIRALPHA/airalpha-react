import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Hello <strong>AIR ALPHA ✌</strong>.
        </p>
      </header>
    </div>
  );
}

export default App;
